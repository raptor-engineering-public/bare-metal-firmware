// © 2020 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

uint32_t micron_n25q_spi_device_ids[] = { 0x20ba2010, 0x20ba2110 };

const char *micron_n25q_spi_device_names[] = { "Micron N25Q 512Mb", "Micron N25Q 1024Mb" };

#define MICRON_N25Q_SPI_FAST_READ_DUMMY_CLOCK_CYCLES 10

#define MICRON_N25Q_SPI_3BA_SPI_READ_CMD 0x03
#define MICRON_N25Q_SPI_4BA_SPI_READ_CMD 0x13

// NOTE: QSPI mode unavailable for single read, use Write Disable command as plaecholder
#define MICRON_N25Q_SPI_3BA_QSPI_READ_CMD 0x04
#define MICRON_N25Q_SPI_4BA_QSPI_READ_CMD 0x04

#define MICRON_N25Q_SPI_3BA_SPI_FAST_READ_CMD  0x0b
#define MICRON_N25Q_SPI_4BA_SPI_FAST_READ_CMD  0x0c
#define MICRON_N25Q_SPI_3BA_QSPI_FAST_READ_CMD 0xeb
#define MICRON_N25Q_SPI_4BA_QSPI_FAST_READ_CMD 0xec

// NOTE: The same command code is used for both QSPI 3BA and QSPI 4BA extended quad input writes, thus the device must be placed in either 3BA or 4BA mode prior
// to issuing PAGE PROGRAM
#define MICRON_N25Q_SPI_3BA_SPI_PAGE_PROGRAM_CMD  0x02
#define MICRON_N25Q_SPI_4BA_SPI_PAGE_PROGRAM_CMD  0x12
#define MICRON_N25Q_SPI_3BA_QSPI_PAGE_PROGRAM_CMD 0x38
#define MICRON_N25Q_SPI_4BA_QSPI_PAGE_PROGRAM_CMD 0x38
