################################################################################
###
### @file       CMakeLists.txt
###
### @project
###
### @brief      Top level CMake file
###
################################################################################
###
################################################################################
###
### @copyright Copyright (c) 2018-2020, Evan Lojewski
### @cond
###
### All rights reserved.
###
### Redistribution and use in source and binary forms, with or without
### modification, are permitted provided that the following conditions are met:
### 1. Redistributions of source code must retain the above copyright notice,
### this list of conditions and the following disclaimer.
### 2. Redistributions in binary form must reproduce the above copyright notice,
### this list of conditions and the following disclaimer in the documentation
### and/or other materials provided with the distribution.
### 3. Neither the name of the copyright holder nor the
### names of its contributors may be used to endorse or promote products
### derived from this software without specific prior written permission.
###
################################################################################
###
### THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
### AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
### IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
### ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
### LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
### CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
### SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
### INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
### CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
### ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
### POSSIBILITY OF SUCH DAMAGE.
### @endcond
################################################################################

enable_testing()
include(GoogleTest)

SET(FPGA_BUILD_DIR "${CMAKE_CURRENT_SOURCE_DIR}/bootrom/fpga/release"
    CACHE PATH
    "Path to the FPGA build folder from the litex-boards repo: <litex-boards-repo>/litex_boards/targets/build/versa_ecp5")

FIND_PROGRAM(CMAKE_C_COMPILER
            NAMES flint_clang clang
            REQUIRED)

FIND_PROGRAM(CMAKE_CXX_COMPILER
            NAMES flint_clang++ clang++
            REQUIRED)

FIND_PROGRAM(CMAKE_ASM_COMPILER
        NAMES clang
        REQUIRED)

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fno-rtti -fno-exceptions")

include(cmake/version.cmake)

cmake_minimum_required(VERSION 3.5.1)
project(firmware VERSION ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH})

include(bootrom/cmake/clang-format.cmake)
include(bootrom/cmake/clang-analyzer.cmake)
include(bootrom/cmake/clang-tidy.cmake)
include(bootrom/cmake/config.cmake)

add_subdirectory(bootrom/utils)
# add_subdirectory(bootrom/tests)

# # Default include for all subprojects
include_directories(${FPGA_BUILD_DIR}/software/include)
include_directories(bootrom/include)

add_definitions("-D__microwatt__")

add_subdirectory(bootrom/libbase)

ppc64le_add_executable(${PROJECT_NAME}
    utility.c       utility.h
    isr.c
    main.c
    fsi.c           fsi.h
    opencores_i2c.c opencores_i2c.h

    aquila.h
    ipmi_bt.h
    micron_n25q_flash.h
    tercel_spi.h
)
ppc64le_linker_script(${PROJECT_NAME} "${CMAKE_CURRENT_SOURCE_DIR}/linker.ld")
target_link_libraries(${PROJECT_NAME} PRIVATE libbase-nofloat)
target_compile_definitions(${PROJECT_NAME} PRIVATE "-DNO_FLINT") # Disable linting

format_target_sources(${PROJECT_NAME})
enable_clang_tidy(${PROJECT_NAME})
